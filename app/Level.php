<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table='levels';
   protected $fillable=['level_id','program_id','level','description'];
   protected $primaryKey='level_id';
   public $timestamps=false;
}
