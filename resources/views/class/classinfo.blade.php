<style type="text/css">
 .acadimic-details{
          white-space: normal;
          width: 400px;
 }
 #table-class-info{
  width: 100%;
 }
 table tbody >tr >td{
  vertical-align: center;
 }

      

</style>
<table class="table-hover table-striped table-condensed table-bordered" id="table-class-info">
   <thead>
        <tr>
        	<th>Program</th>
        	<th>Level</th>
        	<th>Shift</th>
        	<th>Time</th>
        	<th>Academics Details</th>
          <th id="hidden">Action</th>
        </tr>
   </thead>
         <tbody>
         @foreach($classes as $key=>$c)
               <tr>
                    <td>{{$c->program}}</td>
                    <td>{{$c->level}}</td>
                    <td>{{$c->shift}}</td>
                    <td>{{$c->time}}</td>
                    <td class="acadimic-details">
                       <a href="#" data-id="{{$c->class_id}}" id="class-edit">
                       Program:{{$c->program}} / Level:{{$c->level}} / Shift:{{$c->shift}} / Time:{{$c->time}}
                       / Batch:{{$c->batch}} / Group:{{$c->groups}} / StartDate:{{date("d-M-y",strtotime($c->start_date))}}
                        / EndDate: {{date("d-M-y",strtotime($c->end_date))}}
                        </a>
                    </td>
                    <td  style="vertical-align:middle;width: 50px;" id="hidden">
                       <button type="submit" value="{{$c->class_id}}" class="btn btn-danger btn-sm del-class">Del</button>
                       
                    </td>
               </tr>
         @endforeach

         </tbody>
</table>